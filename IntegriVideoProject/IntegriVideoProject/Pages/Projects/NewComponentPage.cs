﻿using IntegriVideoProject.Extensions;
using IntegriVideoProject.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace IntegriVideoProject.Pages.Projects
{
    public class NewComponentPage 
    {
        [FindsBy(How = How.XPath, Using = "//input[@placeholder='New component']")]
        public IWebElement InputComponentName { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='btn']")]
        public IWebElement CreateButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='btn']")]
        public IWebElement ComponentCode { get; set; }

        public void CreateComponent(string componentName)
        {
            Page.Project.AddComponent();
            InputComponentName.EnterText(componentName, "Component name");
            CreateButton.ClickOnIt("Create component button");
        }
    }
}
