﻿using IntegriVideoProject.Extensions;
using IntegriVideoProject.PageObjects;
using IntegriVideoProject.Pages.Billing;
using IntegriVideoProject.WrapperFactory;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace IntegriVideoProject.Pages.Projects
{
    public class ProjectsPage 
    {
        private const string XPATH_PROJECT_LINK = "//div[@id='project-list']/div[@class='row']/div[4]";

        [FindsBy(How = How.XPath, Using = "//div[@class='project new']//a")]
        public IWebElement AddProjectButton { get; set; }

        [FindsBy(How = How.XPath, Using = XPATH_PROJECT_LINK)]
        public IWebElement ProjectLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='New component']")]
        public IWebElement InputComponentName { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='btn']")]
        public IWebElement CreateButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@class='iv-icon iv-icon-credit-card']")]
        public IWebElement LinkBilling { get; set; }


        public MainPage LogOut()
        {
            Page.Login.LogOutIcon.ClickOnIt("Exit button");
            return new MainPage();
        }

        public CreateProjectPage OpenAddProject()
        {
            AddProjectButton.ClickOnIt("Add project button");
            return new CreateProjectPage();
        }

        public ProjectPage OpenProject()
        {
            new WebDriverWait(BrowserFactory.Driver, TimeSpan.FromSeconds(30))
                .Until(ExpectedConditions.ElementExists(By.XPath(XPATH_PROJECT_LINK)));
            ProjectLink.ClickOnIt("Open project");
            return new ProjectPage();
        }

        public BillingPage OpenBilling()
        {
            LinkBilling.ClickOnIt("Open Billing");
            return new BillingPage();
        }
    }
}
