﻿using IntegriVideoProject.Extensions;
using IntegriVideoProject.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace IntegriVideoProject.Pages.Projects
{
    public class ProjectPage 
    {
        [FindsBy(How = How.XPath, Using = "//span[@class='iv-icon iv-icon-file-add']")]
        public IWebElement IconComponentAdd { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Edit')]")]
        public IWebElement EditComponentLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='col-12 description']")]
        public IWebElement ProjectDescription { get; set; }

        public void AddComponent()
        {           
            Page.Projects.OpenProject();
            IconComponentAdd.ClickOnIt("Add component");
        }

        public EditProjectPage OpenEditProject()
        {
            Page.Projects.OpenProject();
            EditComponentLink.ClickOnIt("Edit component");
            return new EditProjectPage();
        }
    }
}
