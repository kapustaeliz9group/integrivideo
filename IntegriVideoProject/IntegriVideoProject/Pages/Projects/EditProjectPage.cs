﻿using IntegriVideoProject.Extensions;
using IntegriVideoProject.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace IntegriVideoProject.Pages.Projects
{
    public class EditProjectPage
    {
        [FindsBy(How = How.XPath, Using = "//textarea[@placeholder='Type here...']")]
        public IWebElement Description { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='btn']")]
        public IWebElement UpdateButton { get; set; }

        public ProjectsPage EditDescription(string description)
        {
            Page.Project.OpenEditProject();
            Description.EnterText(description, "Description");
            UpdateButton.ClickOnIt("Update project button");
            return new ProjectsPage();
        }
    }
}
