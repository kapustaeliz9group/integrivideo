﻿using IntegriVideoProject.Extensions;
using IntegriVideoProject.PageObjects;
using IntegriVideoProject.WrapperFactory;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Configuration;
using System.Linq;

namespace IntegriVideoProject.Pages.Projects
{
    public class CreateProjectPage 
    {

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='New project']")]
        public IWebElement InputProjectName { get; set; }

        [FindsBy(How = How.XPath, Using = "//textarea[@placeholder='Type here...']")]
        public IWebElement InputProjectDiscription { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='example.com']")]
        public IWebElement InputDomain { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='btn']")]
        public IWebElement CreateButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@class='nav-link'][contains(text(),'Projects')]")]
        public IWebElement ProjectsLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@class='nav-link'][contains(text(),'Projects')]")]
        public IWebElement LibraryLink { get; set; }


        public void AddProject(string projectName, string projectDiscription, string domain)
        {
            Page.Projects.OpenAddProject();
            InputProjectName.EnterText(projectName, "Project name");
            InputProjectDiscription.EnterText(projectDiscription, "Project discription");
            InputDomain.EnterText(domain, "Domain");
            CreateButton.ClickOnIt("Add project button");
        }

        public int OpenProjectsPage(string xpathCountProject)
        {
            ((IJavaScriptExecutor)BrowserFactory.Driver).ExecuteScript("window.open()");
            BrowserFactory.Driver.SwitchTo().Window(BrowserFactory.Driver.WindowHandles.Last());
            BrowserFactory.Driver.Url = ConfigurationManager.AppSettings["URL"];
            return BrowserFactory.Driver.FindElements(By.XPath(xpathCountProject)).Count;
        }
    }
}
