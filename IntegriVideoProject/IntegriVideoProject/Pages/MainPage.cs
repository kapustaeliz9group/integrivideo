﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace IntegriVideoProject.Pages
{
    public class MainPage 
    {
        [FindsBy(How = How.XPath, Using = "//a[@class='btn']")]
        public IWebElement LogInButton { get; set; }
    }
}
