﻿using IntegriVideoProject.Extensions;
using IntegriVideoProject.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace IntegriVideoProject.Pages.Billing
{
    public class BillingPage 
    {
        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Add new')]")]
        public IWebElement AddNewCardButton { get; set; }

        public NewCardPage OpenCardPage()
        {
            Page.Projects.OpenBilling();
            AddNewCardButton.ClickOnIt("Add new card button");
            return new NewCardPage();
        }
    }
}
