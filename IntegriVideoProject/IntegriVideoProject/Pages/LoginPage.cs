﻿using IntegriVideoProject.Extensions;
using IntegriVideoProject.Pages.Projects;
using IntegriVideoProject.TestDataAcces;
using IntegriVideoProject.WrapperFactory;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Configuration;

namespace IntegriVideoProject.Pages
{
    public class LoginPage 
    {
        private const string XPATH_EMAIL = "//input[@placeholder='Email']";

        [FindsBy(How = How.XPath, Using = XPATH_EMAIL)]
        [CacheLookup]
        public IWebElement InputEmail { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Password']")]
        [CacheLookup]
        public IWebElement InputPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='btn btn-primary']")]
        [CacheLookup]
        public IWebElement LogInButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Logout')]")]
        [CacheLookup]
        public IWebElement LogOutIcon { get; set; }

        public ProjectsPage LogIn(string testName)
        {
            BrowserFactory.LoadApplication(ConfigurationManager.AppSettings["URL"]);
            var userData = ExcelDataAcces.GetTestData(testName);
            new WebDriverWait(BrowserFactory.Driver, TimeSpan.FromSeconds(10))
                .Until(ExpectedConditions.ElementExists(By.XPath(XPATH_EMAIL)));
            InputEmail.EnterText(userData.Email, "Email");
            InputPassword.EnterText(userData.Password, "password");
            LogInButton.ClickOnIt("Login Button");
            return new ProjectsPage();
        }
    }
}
